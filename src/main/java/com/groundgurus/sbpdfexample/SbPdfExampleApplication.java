package com.groundgurus.sbpdfexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbPdfExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbPdfExampleApplication.class, args);
	}

}
