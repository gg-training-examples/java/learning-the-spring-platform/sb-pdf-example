package com.groundgurus.sbpdfexample.controller;

import com.groundgurus.sbpdfexample.service.PDFReportService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/reports")
public class PdfReportController {
  private final PDFReportService pdfReportService;

  public PdfReportController(PDFReportService pdfReportService) {
    this.pdfReportService = pdfReportService;
  }

  @PostMapping("/pdf")
  public ResponseEntity<byte[]> generatePdf() throws IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_PDF);

    // Here you have to set the actual filename of your pdf
    String filename = randomPdfName();
    headers.setContentDispositionFormData(filename, filename);
    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

    byte[] contents = pdfReportService.generatePdf();
    return new ResponseEntity<>(contents, headers, HttpStatus.OK);
  }

  private String randomPdfName() {
    List<String> numbers = List.of("one", "two", "three", "four", "five");
    return numbers.get(new Random().nextInt(numbers.size())) + "_" + new SimpleDateFormat("MMddyy_hhmmssS").format(new Date()) + ".pdf";
  }
}
