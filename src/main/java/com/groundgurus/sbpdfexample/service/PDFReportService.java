package com.groundgurus.sbpdfexample.service;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class PDFReportService {
  private static final Logger log = LoggerFactory.getLogger(PDFReportService.class);

  public byte[] generatePdf() throws IOException {
    PDDocument document = new PDDocument();
    PDPage page = new PDPage();
    document.addPage(page);

    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
      contentStream.setFont(PDType1Font.COURIER, 12);
      String[][] content = {{"a", "b", "1"},
        {"c", "d", "2"},
        {"e", "f", "3"},
        {"g", "h", "4"},
        {"i", "j", "5"}};
      drawTable(page, contentStream, 700, 100, content);
    } catch (IOException e) {
      log.error(e.getMessage(), e);
    }


    document.save(byteArrayOutputStream);
    document.close();

    InputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    return inputStream.readAllBytes();
  }

  public static void drawTable(PDPage page, PDPageContentStream contentStream,
                               float y, float margin,
                               String[][] content) throws IOException {
    final int rows = content.length;
    final int cols = content[0].length;
    final float rowHeight = 20f;
    final float tableWidth = page.getMediaBox().getWidth() - (2 * margin);
    final float tableHeight = rowHeight * rows;
    final float colWidth = tableWidth / (float) cols;
    final float cellMargin = 5f;

    //draw the rows
    float nexty = y;
    for (int i = 0; i <= rows; i++) {
      contentStream.drawLine(margin, nexty, margin + tableWidth, nexty);
      nexty -= rowHeight;
    }

    //draw the columns
    float nextx = margin;
    for (int i = 0; i <= cols; i++) {
      contentStream.drawLine(nextx, y, nextx, y - tableHeight);
      nextx += colWidth;
    }

    //now add the text
    contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);

    float textx = margin + cellMargin;
    float texty = y - 15;
    for (int i = 0; i < content.length; i++) {
      for (int j = 0; j < content[i].length; j++) {
        String text = content[i][j];
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(textx, texty);
        contentStream.drawString(text);
        contentStream.endText();
        textx += colWidth;
      }
      texty -= rowHeight;
      textx = margin + cellMargin;
    }
  }
}
