# Spring Boot PDF Example
This example shows how to generate a PDF using Apache PDFBox

Please don't use iText if you don't plan to buy their license, their closed source and you need to pay for production use

This seems to be ok for simple things, but hard to use, try checkout Jasper Reports

https://www.baeldung.com/spring-jasper

## References
- https://www.baeldung.com/java-pdf-creation
- http://fahdshariff.blogspot.com/2010/10/creating-tables-with-pdfbox.html#targetText=Creating%20tables%20with%20PDFBox,creating%20tables%20within%20PDF%20documents.
